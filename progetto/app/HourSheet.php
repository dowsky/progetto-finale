<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HourSheet extends Model
{
    protected $fillable = [
        'data',
        'ore',
        'note',
        'id_progetto',
        'id_utente'
    ];


    public function projects(){

        /* il secondo argomento indica quale colonna cercare in Project */
        return $this->belongsTo(Project::class, 'id_progetto'); 
    }
    public function user(){

        /* il secondo argomento indica quale colonna cercare in User */
        return $this->belongsTo(User::class, 'id_utente'); 
    }
}
