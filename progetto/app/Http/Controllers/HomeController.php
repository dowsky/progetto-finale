<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HourSheet;
use Auth;
use DB;
use Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     * a ogni richiesta viene fatta una nuova istanza con controllo autenticazione
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* qusto funziona solo se l'utente è autenticato*/
    public function index()
    {
        return view('welcome');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminhome()
    {
        
        $from = date('Y-m-01');
        $to = date('Y-m-t');
        
        $ore_progetti = DB::select("select nome, sum(ore) as total from projects inner join (select data, ore, id_progetto from hour_sheets where data between '$from' and '$to') as a on projects.id = id_progetto group by id");
        
        $ore_progetti = collect($ore_progetti);
        
        
        $ore_clienti = DB::table('projects')
        ->join('hour_sheets', 'projects.id', '=', 'hour_sheets.id_progetto')
        ->join('clients', 'projects.id_cliente', '=', 'clients.id')
        ->select('id_cliente', 'email', DB::raw('sum(ore) as total'))
        ->groupBy('id_cliente')
        ->get();
        

        $label_progetti = $ore_progetti->pluck('nome');
        $data_progetti = $ore_progetti->pluck('total');

        $label_clienti = $ore_clienti->pluck('email');
        $data_clienti = $ore_clienti->pluck('total');


        $colori1 =[];

        foreach($label_progetti as $l)
        {
            array_push($colori1, '#' . substr(md5(rand()), 0, 6));
        }
        $colori1 = collect($colori1);

        $colori2 =[];

        foreach($label_progetti as $l)
        {
            array_push($colori2, '#' . substr(md5(rand()), 0, 6));
        }
        $colori2 = collect($colori2);

        return view('homes.admin', compact('label_progetti', 'data_progetti', 'label_clienti', 'data_clienti', 'colori1', 'colori2'));
    }

    public function userhome()
    {
        $from = date('Y-m-01');
        $to = date('Y-m-t');

        $hoursheets = HourSheet::where('id_utente', '=', Auth::id())->get();
        $userid = Auth::id();
        $ore_progetti = DB::select("select projects.nome, sum(ore) as total from users inner join (select data, ore, id_utente, id_progetto from hour_sheets where data between '$from' AND '$to' and id_utente = '$userid') as hour_sheets on users.id = id_utente inner join projects on projects.id = hour_sheets.id_progetto group by projects.id");
        $ore_progetti = collect($ore_progetti);

        $label_progetti = $ore_progetti->pluck('nome');
        $data_progetti = $ore_progetti->pluck('total');

        $colori =[];

        foreach($label_progetti as $l)
        {
            array_push($colori, '#' . substr(md5(rand()), 0, 6));
        }
        $colori = collect($colori);

        return view('homes.user', compact('hoursheets', 'label_progetti', 'data_progetti', 'colori'));
    }



    public function adminhomerefresh(Request $request)
    {
        
        $input = $request->all();
        $from = $input['from'];
        $to = $input['to'];

        $ore_progetti = DB::select("select nome, sum(ore) as total from projects inner join (select data, ore, id_progetto from hour_sheets where data between '$from' and '$to') as a on projects.id = id_progetto group by id");
        
        $ore_progetti = collect($ore_progetti);
        
        
        /*$ore_clienti = DB::table('projects')
        ->join('hour_sheets', 'projects.id', '=', 'hour_sheets.id_progetto')
        ->join('clients', 'projects.id_cliente', '=', 'clients.id')
        ->select('id_cliente', 'email', DB::raw('sum(ore) as total'))
        ->groupBy('id_cliente')
        ->get();*/
        $ore_clienti = DB::select("select email, sum(ore) as total from projects inner join (select data, ore, id_progetto from hour_sheets where data between '$from' and '$to') as a on projects.id = id_progetto inner join clients on projects.id_cliente = clients.id group by id_cliente");
        $ore_clienti = collect($ore_clienti);


        $label_progetti = $ore_progetti->pluck('nome');
        $data_progetti = $ore_progetti->pluck('total');

        $label_clienti = $ore_clienti->pluck('email');
        $data_clienti = $ore_clienti->pluck('total');


        $colori1 =[];

        foreach($label_progetti as $l)
        {
            array_push($colori1, '#' . substr(md5(rand()), 0, 6));
        }
        $colori1 = collect($colori1);

        $colori2 =[];

        foreach($label_progetti as $l)
        {
            array_push($colori2, '#' . substr(md5(rand()), 0, 6));
        }
        $colori2 = collect($colori2);

        return view('homes.admin', compact('label_progetti', 'data_progetti', 'label_clienti', 'data_clienti', 'colori1', 'colori2'));
    }



    public function userhomerefresh(Request $request)
    {
        $input = $request->all();
        $from = $input['from'];
        $to = $input['to'];

        $hoursheets = HourSheet::where('id_utente', '=', Auth::id())->where('data', '<=', $to)->where('data', '>=', $from)->get();
        $userid = Auth::id();
        $ore_progetti = DB::select("select projects.nome, sum(ore) as total from users inner join (select data, ore, id_utente, id_progetto from hour_sheets where data between '$from' AND '$to' and id_utente = '$userid') as hour_sheets on users.id = id_utente inner join projects on projects.id = hour_sheets.id_progetto group by projects.id");
        $ore_progetti = collect($ore_progetti);

        $label_progetti = $ore_progetti->pluck('nome');
        $data_progetti = $ore_progetti->pluck('total');

        $colori =[];

        foreach($label_progetti as $l)
        {
            array_push($colori, '#' . substr(md5(rand()), 0, 6));
        }
        $colori = collect($colori);

        return view('homes.user', compact('hoursheets', 'label_progetti', 'data_progetti', 'colori'));
    }
}
