<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class ArchiveController extends Controller
{
    public function index()
    {
        $users = User::where('admin', 0)->get();
        return view ('archives.index', compact('users'));
    }


    public function details(Request $request)
    {
        $input = $request->all();
        if(! empty($input)){
            
            $user = User::find($input['id']);
            $hoursheets = $user->hoursheets->sortByDesc('created_at');
            

        return view('archives.details', compact('hoursheets', 'user'));
        }
    }

    public function userindex()
    {
        ///asf
    }
}
