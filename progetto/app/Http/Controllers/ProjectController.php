<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Client;
use App\User;
use Validator;
use DB;
use Log;    

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $progetti = Project::all();
        return view('projects.index', compact('progetti'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clienti = Client::all();
        return view('projects.create', compact('clienti') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $rules = [
			'costo_orario' => 'required|numeric|between:5,100',
            'descrizione' => 'string|min:3|max:255', 
            'nome' => 'required|string|min:3|max:255',
            'note' => 'string|min:3|max:255',
            'data_inizio' => 'required|date',
            'data_fine' => 'required|date',
            'id_cliente' => 'required|integer|gt:0'
		];

		$messaggio = Validator::make($input, $rules);
		if ($messaggio->fails()) {
            
			return redirect('/projects')->withInput()->withErrors($messaggio);
		}

		else{

			try{
				$progetto = new Project;
                $progetto->nome = $input['nome'];
                $progetto->note = $input['note'];
                $progetto->descrizione = $input['descrizione'];
                $progetto->costo_orario = floatval($input['costo_orario']);
                $progetto->data_inizio_progetto = $input['data_inizio'];
                $progetto->data_prevista_fine_progetto = $input['data_fine'];

                /*$cliente = Client::where ('email', '=', $input['email_cliente'])->first();*/
                $progetto->id_cliente = $input['id_cliente'];

				$progetto->save();
				return redirect('/projects')->with('messaggio', 'Un nuovo progetto è stato inserito con successo!');
			}
			catch(Exception $e){
				return redirect('/projects')->withError("Operazione non andata a buon fine");
			}
		}
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /* non si può chiedere come parametro un oggetto */
    public function edit($id)
    {
        $clienti = Client::all();
        $progetto = Project::find($id);
        return view('projects.edit', compact('progetto', 'clienti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $input = $request->all();

        $rules = [
			'costo_orario' => 'required|numeric|between:5,100',
            'descrizione' => 'string|min:3|max:255', 
            'nome' => 'required|string|min:3|max:255',
            'note' => 'string|min:3|max:255',
            'data_inizio' => 'required|date',
            'data_fine' => 'required|date',
            'id_cliente' => 'required|integer|gt:0'
		];
		$messaggio = Validator::make($input, $rules);
		if ($messaggio->fails()) {
			return redirect('/projects')
			->withInput()
			->withErrors($messaggio);
		}
		else{

			try{
                
                $progetto = Project::find($id);
        
                $progetto->nome= $input['nome'];
                $progetto->descrizione= $input['descrizione'];
                $progetto->note= $input['note'];
                $progetto->costo_orario= floatval($input['costo_orario']);
                $progetto->data_inizio_progetto= $input['data_inizio'];
                $progetto->data_prevista_fine_progetto= $input['data_fine'];
                $progetto->id_cliente = $input['id_cliente'];
                
                $progetto->save();
    
                return redirect('/projects')->with('messaggio', 'Il progetto è stato modificato con successo!');
	
            }
			catch(Exception $e){
				return redirect('/projects')->withError("Operazione non andata a buon fine");
			}
		}

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignProject($id)
    {
        $progetto = Project::find($id);
        
        /*
        $utenti = DB::table('users')
        ->crossJoin('project_user', 'users.id', '=', 'project_user.user_id')
        ->select('users.*')->whereNotIn('project_user.project_id', [$id])
        ->get();

        User::whereNotIn('id', function($query){
            $query->select('paper_type_id')
            ->from(with(new ProductCategory)->getTable())
            ->whereIn('category_id', ['223', '15'])
            ->where('active', 1);
        })->get();*/


        $utenti =DB::table('users')
        ->whereNotIn('id',function ($query) use ($id){
                        $query->select('user_id')->from('projects')
                        ->join('project_user', 'projects.id', '=', 'project_user.project_id')->where('projects.id', '=', $id);
                    })->where('admin', 0)
        ->get();

        return view('projects.assignproject', compact('utenti', 'progetto'));
    }

    /*public function assign($projectid, $userid)
    {
        $user = User::find($userid);
        $project = Project::find($projectid);

        $user->projects()->attach($project->id);

        return redirect('/projects')->with('messaggio', 'Assegnato ' . $project->name . 'a ' . $user->nome . ' ' . $user->cognome . ' con successo!');
    }*/

    public function assign(Request $request)
    {
        $input = $request->all();
        unset($input['_method']);
        unset($input['_token']);

        $p = Project::find($input['id_progetto']);
        unset($input['id_progetto']);
        
        if(! empty($input)){
        
            foreach($input as $user){
                $user = User::find($user);
                $user->projects()->attach($p->id);
                }
        
            return redirect("/projects/details/$p->id")->with('messaggio', 'Assegnamento avvenuto con successo!');
        }
        else{
            return redirect("/projects/details/$p->id");
        }
    }

    public function terminate($projectid)
    {
        $project = Project::find($projectid);

        $project->data_effettiva_fine = date('Y-m-d');

        return redirect('/projects')->with('messaggio', 'Progetto terminato con successo!');
    }


    public function details($projectid)
    {
        $p = Project::find($projectid);
        return view('projects.details', compact('p'));
    }
}
