<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\User;

use Log;
use Validator;

class UserController extends Controller
{

    public function index()
    {
        $users = \App\User::all();
        return view('users.index', compact('users'));
    }


    public function create()
    {
        return view('users.create');
    }


    public function store(Request $request)
    {
        $input = $request->all();

        $rules = [
			'nome' => 'required|string|min:3|max:255',
            'cognome' => 'required|string|min:3|max:255', 
            'email' => 'required|email|min:3|max:255',
            'password' => 'required|string|min:8|max:255',
		];

		$messaggio = Validator::make($input, $rules);
		if ($messaggio->fails()) {
            
			return json_encode(['status' => 'error']);
		}

		else{

			try{

                $user = new User;
                $user->nome = $input['nome'];
                $user->cognome = $input['cognome'];
                $user->email = $input['email'];
                $password =$input['password'];

                $user->admin = isset($input['admin']);

                /*$password = Str::random(8);*/
                
                Log::info($user);

                $user->password = Hash::make($password);
                
                $user->save();

                return json_encode(['status' => 'ok', 'user' => $user]);
            }
            catch(Exception $e){
				return redirect('/users')->withError("Operazione non andata a buon fine");
			}
        }

    }


    public function assignProject($userid, $projectid)
    {

        $user = User::find($userid);
        $project = Project::find($projectid);

        $user->projects()->attach($project);

        return redirect('/users')->with('messaggio', 'Assegnato ' . $project->name . 'a ' . $user->nome . ' ' . $user->cognome . ' con successo!');
    }
}
