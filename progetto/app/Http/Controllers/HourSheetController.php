<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HourSheet;
use App\Project;

use Auth;
use Validator;

use DB;
use Log;

class HourSheetController extends Controller
{


    public function index()
    {
        $hoursheets = HourSheet::where ('id_utente', '=', Auth::id())->get();
        return view('hoursheets.index', compact('hoursheets'));
    }

   

    public function create()
    {
        
        $progetti = DB::table('projects')
        ->join('project_user', 'projects.id', '=', 'project_user.project_id')
        ->where('user_id', '=', Auth::id(), 'and', 'data_effettiva_fine', 'is', 'null')
        ->get();

        return view('hoursheets.create', compact('progetti'));
    }

    
    public function store(Request $request)
    {
        $input = $request->all();

        $rules = [
            'ore' => 'required|numeric|between:1,24',
            'data' => 'required|date',
            'progetto' => 'required|string|min:3|max:255'
        ];
        $messaggio = Validator::make($input, $rules);
		
        if ($messaggio->fails()) {

			return redirect('/userhome')->withInput()->withErrors($messaggio);

		}
        else{
            
            try{

                $hoursheet = new HourSheet;
                $hoursheet->data = $input['data'];
                $hoursheet->ore = $input['ore'];
                $hoursheet->note = $input['note'];
                
                $progetto = Project::where ('nome', '=', $input['progetto'])->first();

                $hoursheet->id_progetto = $progetto->id;
                $hoursheet->id_utente = Auth::id();
                
                $hoursheet->save();
                return redirect('/userhome')->with('success', 'Nuova Scheda Ore aggiunta con successo!');
            
            }
            catch(Exception $e){

				return redirect('/userhome')->withError("Operazione non andata a buon fine");
			}
        }

    }


}
