<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client;
use Validator;

class ClientController extends Controller
{

    public function index()
    {
        $clients = Client::all();
        
        return view ('clients.index', compact('clients'));
    }



    public function create()
    {
        return view('clients.create');
    }


    public function store(Request $request)
    {

        $input = $request->all();

        $rules = [
            'nome' => 'required|string|min:3|max:255',
            'cognome' => 'required|string|min:3|max:255',
            'ragione_sociale' => 'string|min:3|max:255',
            'email' => 'required|email|min:3|max:255'
        ];
        $messaggio = Validator::make($input, $rules);
		
        if ($messaggio->fails()) {

			return redirect('/clients')->withInput()->withErrors($messaggio);

		}
        else{
            
            try{

                Client::create($input);

                return redirect('/clients')->with('messaggio', 'Un nuovo cliente è stato inserito con successo!');
            }
            catch(Exception $e){

				return redirect('/clients')->withError("Operazione non andata a buon fine");
			}
        }
    }


    public function edit($id)
    {
        $client = Client::find($id);
        return view('clients.edit', compact('client'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $rules = [
            'nome' => 'required|string|min:3|max:255',
            'cognome' => 'required|string|min:3|max:255',
            'ragione_sociale' => 'string|min:3|max:255',
            'email' => 'required|email|min:3|max:255'
        ];
        $messaggio = Validator::make($input, $rules);
		
        if ($messaggio->fails()) {

			return redirect('/clients')->withInput()->withErrors($messaggio);

		}
        else{
            
            try{

                $cliente = Client::find($id);

                $cliente->nome= $input['nome'];
                $cliente->cognome= $input['cognome'];
                $cliente->email= $input['email'];
                $cliente->ragione_sociale= $input['ragione_sociale'];
                $cliente->save();
        
                return redirect('/clients')->with('messaggio', 'Il cliente è stato modificato con successo!');
            }
            catch(Exception $e){

				return redirect('/clients')->withError("Operazione non andata a buon fine");
			}
        }
    }

}
