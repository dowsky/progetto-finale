<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class CheckRoleMiddleware
{
    public function handle($request, Closure $next)
    {
        if (! Auth::user()->isAdmin()) {
            return Redirect::to('/')->with('messaggio', '* Autorizzazione negata! *');
        }
        return $next($request);
    }
}
