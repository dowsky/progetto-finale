<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Crypt;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function adminToString() : String
    {
        if($this->admin == 1) {
            return "admin";
        }
        else {
            return "";
        }
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

    public function hoursheets()
    {
        return $this->hasMany(HourSheet::class, 'id_utente');
    }


    public function isAdmin() {
        if($this->admin == 1) {
             return true;
          } else {
             return false;
        }
     
    }

}



/* funzione che controlla se l'utente connesso è admin oppure no
public function isAdmin() {
   if($this->admin == 1) {
        return true;
     } else {
        return false;
   }



public function view () 
    {
        if(Auth::check() && Auth::user()->isAdmin()) {
            $user  = User::all();
            $post  = Post::all();
            $visit = Visits::all();


            return view('admin')->with('post', $post)->with('user', $user)->with('visit', $visit);
        } else {
            return redirect()->to('/');
        }
    }

public function getIsAdminAttribute()
{
   return (bool) $this->admin;
}
*/