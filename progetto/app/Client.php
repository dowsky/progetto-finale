<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/* un modello viene usato per creare istanze di una tabella nel database */
/* viene anche usato in combinazione con ->all() per ottenere tutte le righe dal database */ 


 /*public function update() {

        $article = \App\Article::find(1);

        $article->topic = "Laravel";

        $article->save();

        echo "Update Successful!";

}
*/


class Client extends Model  
{

    /* fillable è una variabile che indica quali campi sono riempibili con un 
    * mass assignment ovvero i valori che ritornano corretta l'istruzione
    * Client::create($input)
    */
    protected $fillable = [
            'ragione_sociale',
            'nome',
            'cognome',
            'email',
    ];


    public function projects(){
        return $this->hasMany('App\Project'); 
    }
}
