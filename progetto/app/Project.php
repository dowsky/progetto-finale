<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'descrizione',
        'nome',
        'note',
        'costo_orario',
        'data_inizio_progetto',
        'data_prevista_fine_progetto',
        'data_effettiva_fine_progetto',
        'id_cliente'
    ];



    public function client(){

        /* il secondo argomento indica quale colonna cercare in Client */
        return $this->belongsTo(Client::class, 'id_cliente'); 
    }

    public function clien2t(){

        /* il secondo argomento indica quale colonna cercare in Client */
        return $this->belongsTo(Client::class, 'id_cliente'); 
    }

    public function difference() : String
    {
        $formatted_dt1=Carbon::today();

        $formatted_dt2=Carbon::parse($this->data_prevista_fine_progetto);

        $ret = $formatted_dt1->diffInDays($formatted_dt2);

        /* ret è sempre maggiore di 0 */
        
        return  "Tempo rimanente: " . $ret . " giorni";
        
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function hoursheets()
    {
        return $this->hasMany(HourSheet::class, 'id_progetto');
    }
    
}
