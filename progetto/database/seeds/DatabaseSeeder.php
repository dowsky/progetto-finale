<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        /* scommentare la riga sotto per far funzionare il comando php artisan db:seed */
        // $this->call(UsersTableSeeder::class);
    }
}
