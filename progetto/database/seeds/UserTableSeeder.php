<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        [
            
            'nome'  => 'percy',
            'cognome' => 'shelley',
            'email' => 'percy@shelley.org',
            'admin' => 1,
            'password' => bcrypt('password'),
            'updated_at' => date('Y-m-d h:i:s'),
            'created_at' => date('Y-m-d h:i:s')
        ],


        ]);
    }
}
