<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HourSheets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hour_sheets', function (Blueprint $table) {
            $table->id('id');
            $table->date('data');
            $table->unsignedInteger('ore');
            $table->string('note')->nullable();

            
            $table->unsignedBigInteger('id_progetto');
            $table->unsignedBigInteger('id_utente');
            /* chiavi esterne */
            $table->foreign('id_progetto')
            ->references('id')
            ->on('projects');
            $table->foreign('id_utente')
            ->references('id')
            ->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoursheets');
    }
}
