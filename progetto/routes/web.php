<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/', 'HomeController@index');

*/


Auth::routes();

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['user', 'auth'])->group(function () {

    Route::get('/userhome', 'HomeController@userhome')->name('userhome');
    Route::resource('/hoursheets', 'HourSheetController');
    Route::post('/userhome', 'HomeController@userhomerefresh');


});

Route::middleware(['admin', 'auth'])->group(function () {

    Route::get('/archive', 'ArchiveController@index');
    Route::post('/archive/details', 'ArchiveController@details');
    /*Route::get('/archive/details/{id}', 'ArchiveController@details');*/

    Route::get('/adminhome', 'HomeController@adminhome')->name('adminhome');
    Route::resource('/users', 'UserController');
    Route::resource('/clients', 'ClientController');
    Route::resource('/projects', 'ProjectController');
    Route::get('/projects/{project}/assignproject', 'ProjectController@assignProject');
    Route::patch('/projects', 'ProjectController@assign');
    Route::post('/adminhome', 'HomeController@adminhomerefresh');
    Route::get('/projects/details/{id}', 'ProjectController@details');
    /*Route::get('/projects/{project}/assignproject/{user}', 'ProjectController@assign');*/
    
    Route::get('/projects/{project}/terminate', 'ProjectController@terminate');

});


Route::fallback(function () {
    /** This will check for the 404 view page unders /resources/views/errors/404 route */
    return view('errors.404');
});