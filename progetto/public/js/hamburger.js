let hamburger = document.querySelector(".hamburger");
    let navLinks = document.querySelector(".nav-links");
    let links = document.querySelectorAll(".nav-links li");

    hamburger.addEventListener('click', ()=>{
        //Animate Links
        navLinks.classList.toggle("nav-active");
        
        links.forEach(link => {
            link.classList.toggle("fade");
        });

        //Hamburger Animation
        hamburger.classList.toggle("toggle");
    });

    /*function rotate()
  {
    /* segno piu 
    plus = document.getElementById("plus");
    plus.classList.toggle("rotate");
  }*/