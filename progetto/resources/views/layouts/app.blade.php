<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    @yield('titolo')
    <!-- Scripts  -->

    <script src="{{ asset('js/jquery-3.2.0.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js" integrity="sha512-hZf9Qhp3rlDJBvAKvmiG+goaaKRZA6LKUO35oK6EsM0/kjPK32Yw7URqrq3Q+Nvbbt8Usss+IekL7CRn83dYmw==" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="{{ asset('css/hamburger.css') }}">
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <style>
        html {
        background-color: #d0e8f2;
        overflow: -moz-scrollbars-vertical; 
        overflow-y: scroll;
      }
      .asterisco-rosso{ color: red }


      .dropdown-menu a:hover{
    color: #333;
    text-decoration: none;
}
    </style>
    <!-- Styles -->
    @yield('stile')
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" style="background-color: #d0e8f2">
        <nav  class="navbar navbar-expand-md navbar-light shadow p-3 mb-5  rounded" style="background-color: #FFF">
            <div class="container">
                <div class="navbar-header" style="font-size: 20px;">
                    @yield('navbar-title')
                    </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('@Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #000;">
                                    {{ Auth::user()->email }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="background-color: #FFF;">
                                    <a style="background-color: #FFF" class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <div class="hamburger">
            <div class="line1"></div>
            <div class="line2"></div>
            <div class="line3"></div>
        </div>
        <div class="nav-links">
            @auth

            @if(Auth::user()->isAdmin())
               <a href="{{ url('/adminhome') }}">{{ __('  Home  ') }}</a>
               @else
               <a href="{{ url('/userhome') }}">{{ __('  Home  ') }}</a>
               @endif
            @else
               <a href="{{ url('/') }}">{{ __('  Home  ') }}</a>
            @endauth
            
            @auth
            @if(Auth::user()->isAdmin())
            <a href="{{ URL::action('ProjectController@index') }}">{{ __('  Progetti  ') }}</a>
            @endif
            @if(Auth::user()->isAdmin())
            <a href="{{ URL::action('ClientController@index') }}">{{ __('  Clienti  ') }}</a>
            @endif
            @if(Auth::user()->isAdmin())
            <a href="{{ URL::action('UserController@index') }}">{{ __('  Utenti  ') }}</a>
            @else
            <a href="{{ URL::action('HourSheetController@create') }}">{{ __('  Nuova Scheda Ore  ') }}</a>
            @endif
            @if(Auth::user()->isAdmin())
            <a href="{{ URL::action('ArchiveController@index') }}">{{ __('  Storico  ') }}</a>
            @endif
            @endauth
        </div>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    @yield('script')
    <script src="{{ asset('js/hamburger.js') }}"></script>
</body>

</html>
