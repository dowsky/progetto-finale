<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>{{ config('app.name') }}</title>
      <!-- Fonts -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
      <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
      <!-- Styles -->
      <style>
         html, body {
         background-color: #c7ffd8;
         color: #000;
         font-family: 'Raleway', sans-serif;
         font-weight: 200;
         height: 100vh;
         margin: 0;
         }
         .full-height {
         height: 100vh;
         }
         .flex-center {
         align-items: center;
         display: flex;
         justify-content: center;
         }
         .position-ref {
         position: relative;
         }
         .top-right {
         position: absolute;
         right: 10px;
         top: 18px;
         }
         .content {
         text-align: center;
         }
         .title {
         font-size: 84px;
         }
         .links > a {
         color: #000;
         padding: 0 25px;
         font-size: 13px;
         font-weight: 600;
         letter-spacing: .1rem;
         text-decoration: none;
         text-transform: uppercase;
         }
         .m-b-md {
         margin-bottom: 30px;
         }
      </style>
      
      
      <script src="{{ asset('js/jquery-3.2.0.min.js')}} "> </script>
   </head>


   <body>
         
        
    <div class="flex-center position-ref full-height">
        <div class="content">
           <div class="title m-b-md text-danger">&lt;&lt;File non trovato&gt;&gt;</div>
            @auth
               
               @if(Auth::user()->isAdmin())
               <h2><a href="{{ url('/adminhome') }}"style="text-decoration: none; ">Torna alla home</a></h2>
               @else
               <h2><a href="{{ url('/userhome') }}" style="text-decoration: none; ">Torna alla home</a></h2>
               @endif

               @else
               <h2><a href="{{ url('/') }}" style="text-decoration: none; ">Torna alla pagina iniziale</a></h2>
            @endauth
        </div>
    </div>
   </body>

</html>