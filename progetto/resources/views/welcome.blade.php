<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>{{ config('app.name') }}</title>
      <!-- Fonts -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
      <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
      <!-- Styles -->
      <style>
         html, body {
         background-color: #d0e8f2;
         color: #000;
         font-family: 'Raleway', sans-serif;
         font-weight: 200;
         height: 100vh;
         margin: 0;
         }
         .full-height {
         height: 100vh;
         }
         .flex-center {
         align-items: center;
         display: flex;
         justify-content: center;
         }
         .position-ref {
         position: relative;
         }
         .top-right {
         position: absolute;
         right: 10px;
         top: 18px;
         }
         .content {
         text-align: center;
         }
         .title {
         font-size: 84px;
         }
         .links > a {
         color: #000;
         padding: 0 25px;
         font-size: 13px;
         font-weight: 600;
         letter-spacing: .1rem;
         text-decoration: none;
         text-transform: uppercase;
         }
         .m-b-md {
         margin-bottom: 30px;
         }
      </style>
      
      
      <script src="{{ asset('js/jquery-3.2.0.min.js')}} "> </script>
   </head>


   <body>
         
        
        @if(Session::has('messaggio') )
         <div class="container">
            <div class="col-sm-12">
               <div class="alert alert-danger alert-dismissible fade show text-center" id="success-alert">{{ Session::get('messaggio') }}</div>
            </div>
         </div>
        @endif

      <div class="flex-center position-ref full-height">
         <div class="content">
            <div class="title m-b-md">Progetto</div>
            
            
        @if (Route::has('login'))
            <div class="links">
               
            @auth
               
               @if(Auth::user()->isAdmin())
                <a href="{{ url('/adminhome') }}">@Home</a>
               @else
                <a href="{{ url('/userhome') }}">@Home</a>
               @endif
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('@Logout') }}</a>
                
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
               
               @else
               <a href="{{ url('/login') }}">@Login</a>

            @endauth
        
            </div> 
            @endif
         </div>
        
      </div>
   </body>



   <script>
      $(document).ready(function(){
          $("#success-alert").delay(1200).slideUp(400, function() {
              $(this).alert('close');
              });
          });
   </script>
</html>