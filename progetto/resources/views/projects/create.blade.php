@extends('layouts.app')

@section('titolo')
    <title>Nuovo Progetto</title>  
@endsection

@section('stile')
<style>.asterisco-rosso{ color: red } </style>
@endsection

@section('content')

@section('navbar-title')Progetti @endsection

<div class="container">
    <h1 class="text-center">Nuovo Progetto</h1>
    <form action={{ URL::action('ProjectController@store')}} method="POST">
        <!-- il valore csrf_field() serve per non permettere l'esecuzione di form non generati da laravel -->
        {{ csrf_field() }}


        <div class="form-group">
            <label>Nome progetto<span class="asterisco-rosso">*</span></label>
            <input type="text" class="form-control" name="nome" required>
        </div>

        <div class="form-row">

        
        
        <div class="form-group col-md-6">
            <label>Cliente referente<span class="asterisco-rosso">*</span></label>
            
            <select class="form-control"  name="id_cliente" required>
            <option selected>...</option>
            
            @foreach ($clienti as $c)
                <option value="{{$c->id}}">{{ $c->nome}} {{$c->cognome}}</option>
            @endforeach
            
            </select>
        
        </div>

        <div class="form-group col-md-6">
            <label>Costo orario<span class="asterisco-rosso">*</span></label>
            
            <select class="form-control" name="costo_orario" required>
            <option selected>...</option>
            
            @for ($i = 5; $i <= 100; $i+=0.5)
                <option value="{{$i}}">{{ $i }}€</option>
            @endfor
            
            </select>
        
        </div>
        
        </div>

        <div class="form-row">        
        
            <div class="form-group col-md-6">
                <label>Data inizio progetto<span class="asterisco-rosso">*</span></label>
                <input type="date" class="form-control datePicker"value={{ date('Y-m-d') }} name="data_inizio" required> 
            </div>
    
            <div class="form-group col-md-6">
                <label>Data prevista fine progetto<span class="asterisco-rosso">*</span></label>
                <input type="date" class="form-control" value={{ date('Y-m-d') }} name="data_fine" required>
            </div>
            
        </div>

        <div class="form-group">
            <label>Descrizione progetto</label>
            <input type="text" class="form-control" name="descrizione">
        </div>
        
        <div class="form-group">
            <label>Note</label>
            <input type="text" class="form-control" name="note">
        </div>
        

        <button type="submit" class="btn btn-primary float-md-right">Aggiungi</button>
    </form>
</div>

@endsection
