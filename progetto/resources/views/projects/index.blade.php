@extends('layouts.app')

@section('titolo')
    <title>Tutti i progetti</title>  
@endsection


@section('content')


@section('navbar-title')Progetti @endsection

<!-- Messaggio di successo se presente -->
@if(Session::has('messaggio') )
<div class="container">
  <div class="col-sm-12">
    <div class="alert alert-success alert-dismissible fade show text-center" id="success-alert">{{ Session::get('messaggio') }}</div>
  </div>
</div>
@endif
<!-- Messaggio di errore se il form non è andato a buon fine -->
@if ($errors->any())
<div class="container">
    <div class="col-sm-12">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
</div>  
@endif





<!-- Titolo e pulsante + -->
<div class="container">
  <div style="background-color: #79a3b1"  class="d-flex  p-3 my-3 rounded shadow-sm">
    <div class="lh-100">
  <h1 class="mb-0 lh-100" style="font-size:32px">
     Tutti i progetti
     <a href="{{ URL::action('ProjectController@create') }}" data-toggle="tooltip" title="Nuovo Progetto">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-clipboard-plus" viewBox="0 0 16 16">
         <path fill-rule="evenodd" d="M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z"/>
         <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/>
         <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/>
       </svg>
     </a>
  </h1>
</div>
</div>
  <hr>
  </div>
  

  <div class="container">
     <div class="row">
      
      
      @foreach ($progetti as $p)
      <div class="card mr-4 mb-5 ml-4 shadow" style="width: 330px; background-color: #eee;">
         <div class="card-body">
           <h5 class="card-title text-center text font-weight-bold" style="font-size: 22px">{{$p->nome}}</h5>
           <p class="card-text text-center">{{$p->descrizione}}</p>
           <p class="card-text text-center">Fine prevista per: {{ date('d/m/Y', strtotime($p->data_prevista_fine_progetto)) }}</p>
           <hr>
           <div style="text-align:center" class="float-md-right"><a  class="btn btn-sm btn-primary"  href="{{URL::action('ProjectController@details', $p->id)}}">Dettagli</a> <a href="{{URL::action('ProjectController@edit', $p)}}" data-toggle="tooltip" title="Modifica Progetto" class="btn btn-sm btn-primary">Modifica</a></div>
         </div>
       </div>
       @endforeach


  </div>
</div>


@endsection


@section('script')
  <script>
    $(document).ready(function(){
        $("#success-alert").delay(1500).slideUp(280, function() {
            $(this).alert('close');
            });
        $('[data-toggle="tooltip"]').tooltip();
        });
  </script>    
@endsection