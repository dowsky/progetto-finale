@extends('layouts.app')

@section('titolo')
    <title>Assegna Utente</title>  
@endsection

@section('stile')
  <style>
    .my-custom-scrollbar {
      position: relative;
      height: 380px;
      overflow: auto;
      }

    .table-wrapper-scroll-y {
      display: block;
      }
  </style>
@endsection


@section('content')

@section('navbar-title')Progetti @endsection

<!-- Titolo e pulsante + -->
<div class="container">
  <div style="background-color: #79a3b1"  class="d-flex  p-3 my-3 rounded shadow-sm">
    <div class="lh-100">
  <h1 style="font-size:30px" class="mb-0">Assegna a <span class="font-weight-bold">{{$progetto->nome}}</span></h1>
    </div>
  </div>
  <hr>
</div>
<div class="container">
  
     <div class="card shadow">

      <div class="card-header" style="background-color: #79a3b1" >
         <h3 class="mb-0" >Utenti Disponibili</h3>
      </div>
<form action="{{ URL::action('ProjectController@assign') }}" method="POST">
  
  @method('PATCH')
  @csrf
  <input type="hidden" name="id_progetto" value="{{$progetto->id}}">

        <div class="table-wrapper-scroll-y my-custom-scrollbar">
        <table class="table table-borderless table-condensed table-hover table-striped" >

          <thead class="text-white" style="background-color: #456268;">
            <tr>
               <th scope="col">Nome</th>
               <th scope="col">Cognome</th>
               <th scope="col">Email</th>
               <th scope="col"></th>
            </tr>
         </thead>

           <tbody>
              @foreach ($utenti as $i => $u)
              <tr>
                 <td>{{ $u->nome }}</td>
                 <td>{{ $u->cognome }}</td>
                 <td>{{ $u->email }}</td>
                 <td><input type="checkbox" name="utente{{$i}}" value="{{$u->id}}"></td>
              </tr>
              @endforeach
           </tbody>
        </table>
        
     </div>
  </div>

  <button type="submit" class="btn btn-primary float-right mt-4">Assegna</button>
</div>
@endsection