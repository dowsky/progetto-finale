@extends('layouts.app')

@section('titolo')
    <title>Modifica Progetto</title>  
@endsection

@section('stile')
<style>.asterisco-rosso{ color: red } </style>
@endsection

@section('content')

@section('navbar-title')Progetti @endsection

<div class="container">
    <h1 class="text-center">Modifica Progetto</h1>
    <form action="{{ URL::action('ProjectController@update', $progetto->id)}}" method="POST">
        <!-- il valore csrf_field() serve per non permettere l'esecuzione di form non generati da laravel -->
        {{csrf_field()}}
        <!-- i metodi PUT/PATCH non sono supportati alla perfezione dai browser per questo si deve modificare a posteriori il metodo del form-->
        @method('PATCH')


        <div class="form-group">
            <label>Nome progetto<span class="asterisco-rosso">*</span></label>
            <input type="text" class="form-control" name="nome" value="{{$progetto->nome}}" required>
        </div>

        <div class="form-row">

        
        
        <div class="form-group col-md-6">
            <label>Cliente referente<span class="asterisco-rosso">*</span></label>
            
            <select class="form-control"  name="id_cliente" required>
                <option selected>{{ $progetto->client->nome}} {{$progetto->client->cognome}}</option>
                
                @foreach ($clienti as $c)
                    <option value="{{$c->id}}">{{ $c->nome}} {{$c->cognome}}</option>
                @endforeach
                
                </select>
        
        </div>

        <div class="form-group col-md-6">
            <label>Costo orario<span class="asterisco-rosso">*</span></label>
            
            <select class="form-control" name="costo_orario"  required>
            <option selected>{{$progetto->costo_orario}}€</option>
            
            @for ($i = 5; $i <= 100; $i+=0.5)
                <option>{{ $i }}€</option>
            @endfor
            
            </select>
        
        </div>
        
        </div>

        <div class="form-row">        
        
            <div class="form-group col-md-6">
                <label>Data inizio progetto<span class="asterisco-rosso">*</span></label>
                <input type="date" class="form-control datePicker" name="data_inizio" value="{{$progetto->data_inizio_progetto}}" required> 
            </div>
    
            <div class="form-group col-md-6">
                <label>Data prevista fine progetto<span class="asterisco-rosso">*</span></label>
                <input type="date" class="form-control" name="data_fine" value="{{$progetto->data_prevista_fine_progetto}}" required>
            </div>
            
        </div>

        <div class="form-group">
            <label>Descrizione progetto</label>
            <input type="text" class="form-control" value="{{$progetto->descrizione}}" name="descrizione">
        </div>
        
        <div class="form-group">
            <label>Note</label>
            <input type="text" class="form-control" value="{{$progetto->note}}" name="note">
        </div>
        

        <button type="submit" class="btn btn-primary">Modifica</button>
    </form>
</div>

@endsection
