@extends('layouts.app')

@section('titolo')
    <title>{{$p->nome}}</title>  
@endsection

@section('stile')
  <style>
    .my-custom-scrollbar {
      position: relative;
      height: 380px;
      overflow: auto;
      }

    .table-wrapper-scroll-y {
      display: block;
      }
  </style>  
@endsection


@section('content')


@section('navbar-title')Progetti @endsection


<!-- Messaggio di successo se presente -->
@if(Session::has('messaggio') )
<div class="container">
  <div class="col-sm-12">
    <div class="alert alert-success alert-dismissible fade show text-center" id="success-alert">{{ Session::get('messaggio') }}</div>
  </div>
</div>
@endif
<!-- Messaggio di errore se il form non è andato a buon fine -->
@if ($errors->any())
<div class="container">
    <div class="col-sm-12">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
</div>  
@endif





<div class="container mt-3">
    <div class="card shadow">
        <div class="card-header" style="background-color: #79a3b1"><h3 class="mb-0">Dettagli</h3></div>

        <div class="card-body">
          <div class="row">
            <div class="col-6"><p><span class="font-weight-bold">Nome:</span> {{$p->nome}}</p></div>
            <div class="col-6"><p><span class="font-weight-bold">Descrizione:</span>  {{$p->descrizione}}</p></div>
            <hr>
          </div>
          <div class="row">
            <div class="col-6"><span class="font-weight-bold">Note:</span>  {{$p->note}}</p></div>
            <div class="col-6"><p><span class="font-weight-bold">Costo orario:</span>  {{$p->costo_orario}}€</p></div>
            <hr>
          </div>
        
          <div class="row">
            <div class="col-6"><p><span class="font-weight-bold">Inizio:</span>  {{ date('d/m/Y', strtotime($p->data_inizio_progetto)) }}</p></div>
            <div class="col-6"><p><span class="font-weight-bold">Prevista fine:</span>  {{ date('d/m/Y', strtotime($p->data_prevista_fine_progetto)) }}</p></div>
          </div>
          <div class="row">
            <div class="col-6"><p><span class="font-weight-bold">Effettiva fine:</span>  {{ isset($p->data_effettiva_fine_progetto) ? date('d/m/Y', strtotime($p->data_effettiva_fine_progetto)) : '' }}</p></div>
            <div class="col-6"><p><span class="font-weight-bold">Tempo rimanente:</span>  {{ (strtotime($p->data_prevista_fine_progetto)-strtotime(date('m/d/Y'))) / 86400 }} giorni</p></div>
          </div>
        </div>
    </div>
  <hr>
    </div>
    
 


 
<div class="container mt-3">
<div class="card shadow">

    <div class="card-header" style="background-color: #79a3b1"><h3>Utenti assegnati al progetto
        <a href="{{ URL::action('ProjectController@assignProject', $p->id) }}" data-toggle="tooltip" title="Aggiungi Utente">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16">
                <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
              </svg>
         </a>
        </h3>
    </div>
    <div class="table-wrapper-scroll-y my-custom-scrollbar">
    <table class="table table-borderless table-condensed table-hover table-striped">
       <thead class="text-white" style="background-color: #456268;">
          <tr>
             <th scope="col">Nome</th>
             <th scope="col">Cognome</th>
             <th scope="col">Email</th>
             <th scope="col">Ore</th>
          </tr>
       </thead>
       <tbody>
          @foreach($p->users as $u)
          <tr>
             <td>{{$u->nome}}</td>
             <td>{{$u->cognome}}</td>
             <td>{{$u->email}}</td>
             <td>{{$u->hoursheets->where('id_progetto', '=', $p->id)->sum('ore')}}</td>
          </tr>
          @endforeach
       </tbody>
    </table>

 </div>

 <div class="card-footer text-muted">
    <p class="text-right mr-3">Totale ore spese sul progetto: {{$p->hoursheets->sum('ore')}} ore</p>   
 </div>
</div>
</div>







@section('script')
  <script>
    $(document).ready(function(){
        $("#success-alert").delay(1500).slideUp(280, function() {
            $(this).alert('close');
            });
        $('[data-toggle="tooltip"]').tooltip();
        });
  </script>    
@endsection



 @endsection