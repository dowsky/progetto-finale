@extends('layouts.app')

@section('titolo')
    <title>Tutti gli utenti</title> 
@endsection

@section('stile')
  <style>
    .my-custom-scrollbar {
      position: relative;
      height: 380px;
      overflow: auto;
      }

    .table-wrapper-scroll-y {
      display: block;
      }
  </style>
@endsection

@section('content')

@section('navbar-title')Utenti @endsection


<div class="container" id="alert_placeholder"></div>

<!-- Titolo  -->
<div class="container"> 
  <div style="background-color: #79a3b1"  class="d-flex  p-3 my-3 rounded shadow-sm">
    <div class="lh-100">
      <h1 class="mb-0 lh-100" style="font-size:32px">Tutti gli utenti</h1>
    </div>
  </div>
  <hr >
</div>


<!-- lista utenti -->

<div class="container">
  <div class="card shadow" >
  <div class="table-wrapper-scroll-y my-custom-scrollbar">
  
      <table class="table table-striped table-hover" id="user-table">
        
        <thead class="text-white" style="background-color: #456268">
          <tr>
            
            <th scope="col">Nome</th>
            <th scope="col">Cognome</th>
            <th scope="col" >Email</th>
            <th scope="col">Admin</th>
          </tr>
        </thead>


        <tbody>

        @foreach ($users as $u)
          <tr>
            <td>{{ $u->nome }}</td>
            <td>{{ $u->cognome }}</td>
            <td>{{ $u->email }}</td>
            <td style="color:red;">{{ $u->adminToString()}}</td>
          </tr>
        @endforeach
        
        </tbody>
      
    </table>
  </div>
</div>
</div>



<!-- Aggiungi utente con AJAX -->

<div class="container mt-5">
  <div class="accordion" id="accordion"  aria-multiselectable="true">
        
    
    
    <div class="card shadow" style="background-color: #FFF" >
        
      
        <!-- Card header -->
        <div class="card-header" style="cursor: pointer; background-color: #79a3b1;" data-toggle="collapse" data-target="#collapse" class="mb-0 btn-link">
            
            <div class="link col-12" data-toggle="tooltip" title="Dettagli" >
                <h3 id="titolo-accordion"> Aggiungi Utente
                <svg class="mb-0 btn-link" style="cursor: pointer" data-toggle="collapse" data-target="#collapse"  xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16">
                  <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                  <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                </svg>
              </h3>
            </div>  
        </div>
        
        
        
        <!-- Card body -->
        <div id="collapse" class="collapse" data-parent="#accordion">
          
            <div class="card-body">
                <div class="container-md "> 

                      <div class="form-group">
                        <label>Nome<span class="asterisco-rosso">*</span></label>
                        <input type="text" class="form-control" name="nome" id="nome" required>
                    </div>
                      
                    <div class="form-group">
                        <label>Cognome<span class="asterisco-rosso">*</span></label>
                        <input type="text" class="form-control" name="cognome" id="cognome" required>
                    </div>
                    
                    <div class="form-group">
                      <label>Password<span class="asterisco-rosso">*</span></label>
                      <input type="password" class="form-control" name="password" id="password" autocomplete="new-password">
                      
                    </div>

                    <div class="form-group">
                      <label>Indirizzo email<span class="asterisco-rosso">*</span></label>
                      <input type="email" class="form-control" name="email" id="email">
                      <small class="form-text text-muted">esempio@mail.org</small>
                    </div>
                    
                    <div class="form-group form-check">
                      <input class="form-check-input" name="admin" type="checkbox" id="admin" >
                      <label class="form-check-label" ><span class="asterisco-rosso">Premere se l'utente da registrare è un admin</span></label>
                    </div>
                    
                    <div class="col-md-2">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    </div>
                
                
      
            <hr>
            <button type="button" class="btn btn-sm btn-primary" id="add-user">Aggiungi</button>

          </div>
        </div>
        </div>

</div>

</div>
</div>
@endsection

@section('script')

<script>

  $('document').ready(function() {
      $('#add-user').bind('click', function(e) {
          e.preventDefault();

          var nome = $('#nome').val();
          var cognome = $('#cognome').val();
          var email = $('#email').val();
          var password = $('#password').val();
          var admin;
          if ($("#admin").is(':checked')) {
              admin = true;
          }


          var _token = $('#_token').val();

          if (nome.length > 0 && cognome.length > 0 && email.length > 0) {
              $.ajax({
                  url: "users",
                  type: "POST",
                  dataType: "json",
                  data: {
                      'nome': nome,
                      'cognome': cognome,
                      'email': email,
                      'password': password,
                      'admin': admin,
                      '_token': _token
                  },
                  success: function(data) {
                      if (data.status === 'ok') {

                          var newColName = $('<td/>', {
                              text: data.user.nome
                          });
                          var newColEmail = $('<td/>', {
                              text: data.user.email
                          });
                          var newColSurname = $('<td/>', {
                              text: data.user.cognome
                          });

                          var newColAdmin;
                          if (data.user.admin) {
                              newColAdmin = document.createElement('td');
                              newColAdmin.style = 'color:red;';
                              newColAdmin.textContent = 'admin';
                          } else {
                              newColAdmin = document.createElement('td');
                          }

                          var newRow = $('<tr/>').append(newColName).append(newColSurname).append(newColEmail).append(newColAdmin);
                          $('#user-table').append(newRow);

                          $('#collapse').removeClass('show');

                          $('#alert_placeholder').html('<div class="alert alert-success alert-dismissible fade show text-center" id="success-alert">Utente creato con successo!</div>').delay(1500).slideUp(280, function() {
                          $(this).alert('close');
                          });

                      }
                      else {
                      $('#collapse').removeClass('show');
                      $('#alert_placeholder').html('<div class="alert alert-danger alert-dismissible fade show text-center" id="success-alert">Si è verificato un errore.</div>').delay(1500).slideUp(280, function() {
                          $(this).alert('close');
                          });
                        }
                  },
                  error: function(response, stato) {
                    
                  }
              });
          }
      });

  });
</script>

@endsection
