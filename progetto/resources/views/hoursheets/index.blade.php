@extends('layouts.app')

@section('titolo')
    <title>Schede Ore</title> 
@endsection

@section('content')

<!-- Messaggio di successo se presente -->
@if(Session::has('messaggio') )
<div class="container">
  <div class="col-sm-12">
    <div class="alert alert-success alert-dismissible fade show text-center" id="success-alert">{{ Session::get('messaggio') }}</div>
  </div>
</div>
@endif

<!-- Messaggio di errore se il form non è andato a buon fine -->
@if ($errors->any())
<div class="container">
    <div class="col-sm-12">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
</div>  
@endif


<!-- Titolo e pulsante + -->
<div class="container"> 
  <h1 class="mb-3">Le Tue Schede Ore
    <a href="{{ URL::action('HourSheetController@create') }}" data-toggle="tooltip" title="Nuova Scheda Ore">
      <svg width="28" height="28" fill="#0d6efd" class="bi bi-plus-circle" viewBox="0 0 16 16">
      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" ></path>
      <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"></path>
      </svg>
    </a>
  </h1>
  <hr>
</div>



<div class="container">

  <div class="card">
      <table class="table table-striped">
        
        <thead class="thead-dark">
          <tr>
            <th scope="col">Creato il</th>
            <th scope="col">Progetto</th>
            <th scope="col" >Ore impiegate</th>
            <th scope="col">Data</th>
            <th scope="col">Note</th>
          </tr>
        </thead>


        <tbody>

        @foreach ($hoursheets as $h)
          <tr>
            <td>{{ $h->created_at }}</td>
            <td>{{ $h->projects->nome }}</td>
            <td>{{ $h->ore }}</td>
            <td>{{ $h->data }}</td>
            <td>{{ $h->note }}</td>
          </tr>
        @endforeach
        
        </tbody>
      
    </table>
  </div>
</div>

@endsection



@section('script')
  <script>
    $(document).ready(function(){
        $("#success-alert").delay(1500).slideUp(280, function() {
            $(this).alert('close');
            });
        $('[data-toggle="tooltip"]').tooltip();
        });
  </script>    
@endsection