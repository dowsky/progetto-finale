@extends('layouts.app')

@section('titolo')
  <title>Nuova Scheda Ore</title>  
@endsection

@section('stile')
  <style>.asterisco-rosso{ color: red } </style>
@endsection

@section('content')

@section('navbar-title')Schede Ore @endsection

<div class="container">
    <h1 class="text-center">Nuova Scheda Ore</h1>
    <div class="row">
      
      
      <div class="col-sm">
      </div>
        
        
      <div class="col-sm">
        
          <form action="{{ URL::action('HourSheetController@store') }}" method="POST">
          <!-- il valore csrf_field() serve per non permettere l'esecuzione di form non generati da laravel -->
          {{csrf_field()}}

          <div class="form-group">
            <label>Progetto<span class="asterisco-rosso">*</span></label>
            <select class="form-control"  name="progetto" required>
              <option selected>...</option>
              
              @foreach ($progetti as $p)
                  <option>{{ $p->nome}}</option>
              @endforeach
              
              </select>
          </div>

          <div class="form-group">
            <label>Ore impiegate nella giornata<span class="asterisco-rosso">*</span> </label>
            <input type="number" class="form-control" name="ore" required>
          </div>

          <div class="form-group">
            <label>Data<span class="asterisco-rosso">*</span> </label>
            <input type="date" class="form-control" value={{ date('Y-m-d') }} name="data" required>
          </div>

            <div class="form-group">
              <label for="nome">Note</label>
              <input type="text" class="form-control" name="note">
            </div>

            <button type="submit" id="bottone" class="btn btn-primary">Aggiungi</button>
          </form>
        
      </div>
        
        
        <div class="col-sm">
        </div>
    </div>
</div>

@endsection