@extends('layouts.app')

@section('content')

<div class="container">
    <h1 class="text-center">Modifica Scheda Ore</h1>
    <div class="row">
        <div class="col-sm">
        </div>
        <div class="col-sm">
    <form id="form" action="{{ URL::action('HourSheetController@store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="nome">Data</label>
            <input type="date" class="form-control" value={{ date('Y-m-d') }}>
          </div>
          <div class="form-group">
            <label for="cognome">Progetto</label>
            <input type="text" class="form-control">
          </div>
          <div class="form-group">
            <label for="nome">Ore impiegate nella giornata</label>
            <input type="text" class="form-control">
          </div>
          <div class="form-group">
            <label for="nome">Note</label>
            <input type="text" class="form-control" placeholder="può essere lasciato in bianco...">
          </div>
          <button type="submit" id="bottone" class="btn btn-primary">Invia</button>
        </form>
        </div>
        <div class="col-sm">
        </div>
    </div>
</div>

@endsection