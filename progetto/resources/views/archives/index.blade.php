@extends('layouts.app')


@section('titolo')
    <title>Tutti gli utenti</title> 
@endsection

@section('stile')
  <style>
    .my-custom-scrollbar {
      position: relative;
      height: 380px;
      overflow: auto;
      }

    .table-wrapper-scroll-y {
      display: block;
      }
  </style>
@endsection


@section('content')

@section('navbar-title')Storico @endsection

<div class="container">

    <div class="card shadow" style="background-color: #FFF">
       
      <div class="card-header" style="background-color: #79a3b1"><h3 class="mb-0">Seleziona Utente</h3></div>

       <div class="card-body">
          <form action="{{ URL::action('ArchiveController@details') }}" method="POST">
            @csrf
            <div class="row">
             <div class="col-12 form-group form-inline">
   
               <select class="form-control col-8" name="id" required>
                   <option selected>...</option>
                   @foreach($users as $u)
                   <option value="{{$u->id}}">{{ $u->nome }} {{ $u->cognome }}</option>
                   @endforeach
                </select>
                
                <button type="submit" id="bottone" class="btn btn-primary ml-4">Vai</button>
             </div>
            </div>
          </form>
       </div>
    </div>
 </div>


@endsection