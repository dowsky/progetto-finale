
@extends('layouts.app')


@section('titolo')
    <title>Dettagli utente</title> 
@endsection

@section('stile')
  <style>
    .my-custom-scrollbar {
      position: relative;
      height: 600px;
      overflow: auto;
      }

    .table-wrapper-scroll-y {
      display: block;
      }
  </style>
@endsection


@section('content')

@section('navbar-title')Storico @endsection

<!-- Titolo  -->
<div class="container"> 
  <div style="background-color: #79a3b1"  class="d-flex  p-3 my-3 rounded shadow-sm">
    <div class="lh-100">
      <h1 class="mb-0 lh-100" style="font-size:30px">Storico di <span class="font-weight-bold">{{$user->nome}} {{$user->cognome}}</span></h1>
    </div>
  </div>
  <hr >
</div>

<div class="container">
    <div class="card shadow" >
    <div class="table-wrapper-scroll-y my-custom-scrollbar">
      <table class="table table-striped table-hover" id="user-table">
          <thead class="text-white" style="background-color: #456268">
            <tr>
              <th scope="col">Creato il</th>
              <th scope="col">Creato alle</th>
              <th scope="col">Nome Progetto</th>
              <th scope="col" >Ore impiegate</th>
              <th scope="col">Data</th>
              <th scope="col">Note</th>
            </tr>
          </thead>
  
  
          <tbody>
  
          @foreach ($hoursheets as $h)
            <tr>
              <td>{{ date('d/m/Y', strtotime($h->created_at)) }}</td>
              <td>{{ date('H:i:s', strtotime($h->created_at)) }}</td>
              <td>{{ $h->projects->nome }}</td>
              <td>{{ $h->ore }}</td>
              <td>{{ date('d/m/Y', strtotime($h->data)) }}</td>
              <td>{{ $h->note }}</td>
            </tr>
          @endforeach
          
          </tbody>
        
      </table>
    </div>
  </div>
  </div>

  @endsection