@extends('layouts.app')

@section('titolo')
    <title>Home Admin</title>
@endsection





@section('content') 

@section('navbar-title')Home Admin @endsection


<!-- selezione data -->
<div class="container">
    <div class="card shadow" style="background-color: #FFF">

        <div class="card-header" style="background-color: #79a3b1"><h3 class="mb-0">Seleziona Data</h3></div>

       <div class="card-body">
<form action="{{ URL::action('HomeController@adminhomerefresh') }}" method="POST">
          <div class="form-group form-inline">
             <div class="form-inline form-group mr-5">
                <label>Data inizio</label>
                <input type="date" class="ml-2 form-control" name="from" value="{{ date('Y-m-01') }}">
             </div>
             <div class="form-group mr-5">
                <label>Data fine</label>
                <input type="date" class="ml-2 form-control" name="to" value="{{ date('Y-m-t') }}">
             </div>
             <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
             <button type="submit" id="bottone" class="btn btn-primary">Aggiorna</button>
        </form>
          </div>

       </div>

    </div>
    <hr>
 
</div>
    
<!-- grafico -->
<div class="container mt-3">
    <div class="card shadow">

        <div class="card-header" style="background-color: #79a3b1"><h3 class="mb-0">Statistiche</h3></div>

        <div class="card-body">
    <div class="row">
       <canvas id="myChart1" class="col-md-6"></canvas>
       <canvas id="myChart2" class="col-md-6"></canvas>
    </div>
</div>
    </div>
 </div>

    <script>
 /*
$('document').ready(function() {

        $.ajax({
             url: "users",
             type: "POST",
             dataType: "json",
             data: {
                 'nome': nome,
                 'cognome': cognome,
                 'email': email,
                 'admin': admin,
                 '_token': _token
             },
             success: function(data) {
                 if (data.status === 'ok') {
                 var newColName = $('<td/>', {
                         text: data.user.nome
                     });
                     var newColEmail = $('<td/>', {
                         text: data.user.email
                     });
                     var newColSurname = $('<td/>', {
                         text: data.user.cognome
                     });
                 var newColAdmin;
                     if (data.user.admin) {
                         newColAdmin = document.createElement('td');
                         newColAdmin.style = 'color:red;';
                         newColAdmin.textContent = 'admin';
                     } else {
                         newColAdmin = document.createElement('td');
                     }
                 var newRow = $('<tr/>').append(newColName).append(newColSurname).append(newColEmail).append(newColAdmin);
                     $('#user-table').append(newRow);
                 }
             },
             error: function(response, stato) {
                 console.log(stato);
             }
         });
          }
      });

  });
*/

let RandomColors = ["#" + ((1<<24)*Math.random() | 0).toString(16), "#" + ((1<<24)*Math.random() | 0).toString(16) ];

let myDoughnutChart1 = document.getElementById("myChart1").getContext('2d');

let chart1 = new Chart(myDoughnutChart1, {
    type: 'bar',
    data: {
        labels: {!!json_encode($label_progetti)!!},
        datasets: [{
            barPercentage: 0,
            barThickness: 30,
            maxBarThickness: 50,
            minBarLength: 2,
            data: {!! json_encode($data_progetti)!!},
            backgroundColor: {!!json_encode($colori1)!!}
        }]
    },
    options: {
        legend: {
        display: false
    },
        title: {
            text: "Ore spese su ogni progetto",
            display: true,
            fontFamily : "Raleway",
            fontStyle: 'bold',
            fontSize: '22',
            fontColor: '#000000'
        },
        scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
    }
});

let myDoughnutChart2 = document.getElementById("myChart2").getContext('2d');
RandomColors = ["#" + ((1<<24)*Math.random() | 0).toString(16)
, "#" + ((1<<24)*Math.random() | 0).toString(16)
];


let chart2 = new Chart(myDoughnutChart2, {
    type: 'pie',
    data: {
        labels: {!! json_encode($label_clienti)!!},
        datasets: [ {
            data: {!! json_encode($data_clienti)!!},
            backgroundColor: {!!json_encode($colori2)!!}
        }]
    },
    options: {
        title: {
            text: "Ore spese per ogni cliente",
            display: true,
            fontFamily : "Raleway",
            fontStyle: 'bold',
            fontSize: '22',
            fontColor: '#000000'
        }
    }
});


        
        </script>

@endsection
