@extends('layouts.app')

@section('titolo')
    <title>Home Utente</title> 
@endsection

@section('stile')
    <style>
	canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}

    .my-custom-scrollbar {
      position: relative;
      height: 300px;
      overflow: auto;
      }

    .table-wrapper-scroll-y {
      display: block;
      }
</style>
@endsection

@section('content')

@section('navbar-title')Home Utente @endsection

<!-- Messaggio di successo se presente -->
@if(Session::has('messaggio') )
<div class="container">
  <div class="col-sm-12">
    <div class="alert alert-success alert-dismissible fade show text-center" id="success-alert">{{ Session::get('messaggio') }}</div>
  </div>
</div>
@endif


<!-- Messaggio di errore se il form non è andato a buon fine -->
@if ($errors->any())
<div class="container">
    <div class="col-sm-12">
    <div class="alert alert-danger text-center" id="error-alert">
            @foreach ($errors->all() as $error)
                {{ $error }}
            @endforeach
    </div>
</div>
</div>  
@endif


<!-- selezione data -->

<div class="container">
    <div class="card shadow" >
       
        <div class="card-header" style="background-color: #79a3b1"><h3 class="mb-0">Seleziona Data</h3></div>

       <div class="card-body">
        <form action="{{ URL::action('HomeController@userhomerefresh') }}" method="POST">
          <div class="form-group form-inline">
             <div class="form-inline form-group mr-5">
                <label>Data inizio</label>
                <input type="date" class="ml-2 form-control" name="from" value="{{ date('Y-m-01') }}">
             </div>
             <div class="form-group mr-5">
                <label>Data fine</label>
                <input type="date" class="ml-2 form-control" name="to" value="{{ date('Y-m-t') }}">
             </div>
             <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
             <button type="submit" class="btn btn-sm btn-primary">Aggiorna</button>
          </div>

       </div>

    </div>
    <hr>
 </div>


<div class="container mt-3">
    <div class="card shadow" >



<!-- scheda ore -->
<div class="card-header" style="background-color: #79a3b1">
  <h3 class="mb-0">Le Tue Schede Ore
    <a href="{{ URL::action('HourSheetController@create') }}" data-toggle="tooltip" title="Nuova Scheda Ore">
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-calendar-plus" viewBox="0 0 16 16">
            <path d="M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z"/>
            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
          </svg>
    </a>
  </h3>
</div>


<div class="table-wrapper-scroll-y my-custom-scrollbar">

  <table class="table table-borderless table-condensed table-hover table-striped">
        
        <thead class="text-white" style="background-color: #456268">
          <tr>
            
            <th scope="col">Progetto</th>
            <th scope="col">Data</th> 
            <th scope="col">Ore impiegate</th>
            <th scope="col">Note</th>
            <!--<th scope="col">Data di creazione</th>
            <th scope="col">Ora di creazione</th>-->
          </tr>
        </thead>


        <tbody>

        @foreach ($hoursheets as $h)
          <tr>
            
            <td>{{ $h->projects->nome }}</td>
            <td>{{ date('d/m/Y', strtotime($h->data)) }}</td>
            <td>{{ $h->ore }}h</td>
            <td>{{ $h->note }}</td>
<!--            <td>{{ date('d/m/Y', strtotime($h->created_at)) }}</td>
            <td>{{ date('h:i:s', strtotime($h->created_at)) }}</td>-->
          </tr>
        @endforeach
        
        </tbody>
      
    </table>
  </div>
</div>
<hr>
</div>


<!-- grafico -->
<div class="container mt-3">
    <div class="card shadow" >

        <div class="card-header" style="background-color: #79a3b1">
            <h3 class="mb-0">Statistiche</h3>
        </div>

        <div class="card-body">
    <div class="row">
      <div class="col-3"></div>
      <div class="col-6">
       <canvas id="myChart1" ></canvas></div>
       <div class="col-3"></div>
    </div>
</div>
    </div>
 </div>

@endsection



@section('script')
  <script>
    $(document).ready(function(){
        $("#success-alert").delay(1500).slideUp(280, function() {
            $(this).alert('close');
            });
            $("#error-alert").delay(1500).slideUp(280, function() {
            $(this).alert('close');
            });
        $('[data-toggle="tooltip"]').tooltip();
        });



/* grafico */

let colors1 = ['#49A9EA', '#36CAAB'];

let myDoughnutChart1 = document.getElementById("myChart1").getContext('2d');

let chart1 = new Chart(myDoughnutChart1, {
    type: 'pie',
    data: {
        labels: {!!json_encode($label_progetti)!!},
        datasets: [{
            barPercentage: 0,
            barThickness: 30,
            maxBarThickness: 50,
            minBarLength: 2,
            data: {!! json_encode($data_progetti)!!},
            backgroundColor: {!!json_encode($colori)!!}
        }]
    },
    options: {
        title: {
            text: "Ore spese su ogni progetto",
            display: true,
            fontFamily : "Raleway",
            fontStyle: 'bold',
            fontSize: '22',
            fontColor: '#000000'
        },
    }
});

  </script>    
@endsection