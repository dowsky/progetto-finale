@extends('layouts.app')

@section('titolo')
  <title>Modifica Cliente</title>  
@endsection

@section('stile')
  <style>.asterisco-rosso{ color: red } </style>
@endsection

@section('content')

@section('navbar-title')Modifica Cliente @endsection

<div class="container">
    <h1 class="text-center">Modifica Cliente</h1>
    <div class="row">

      
        <div class="col-sm">
        </div>


          <div class="col-sm">
            <form id="form" action="{{ URL::action('ClientController@update', $client)}}" method="POST">
              
              <!-- il valore csrf_field() serve per non permettere l'esecuzione di form non generati da laravel -->
              {{csrf_field()}}
              <!-- i metodi PUT/PATCH non sono supportati alla perfezione dai browser per questo si deve modificare a posteriori il metodo del form-->
              @method('PATCH')

              <div class="form-group">
                  <label>Nome cliente<span class="asterisco-rosso">*</span></label>
                  <input type="text" class="form-control" name="nome" value="{{ $client->nome }}" required>
                </div>
                <div class="form-group">
                  <label>Cognome cliente<span class="asterisco-rosso">*</span></label>
                  <input type="text" class="form-control" name="cognome" value="{{ $client->cognome }}" required> 
                </div>
                <div class="form-group">
                  <label>Ragione sociale</label>
                  <input type="text" class="form-control" name="ragione_sociale" value="{{ $client->ragione_sociale }}">
                </div>
                <div class="form-group">
                  <label>Indirizzo email<span class="asterisco-rosso">*</span></label>
                  <input type="email" class="form-control" name="email" value="{{ $client->email }}" required>
                  <small class="form-text text-muted">esempio@mail.org</small>
                </div>
                <button type="submit" id="bottone" class="btn btn-primary">Modifica</button>
            </form>
          </div>


        <div class="col-sm" >
        </div>

        
    </div>
</div>
@endsection
