@extends('layouts.app')

@section('titolo')
    <title>Tutti i clienti</title> 
@endsection

@section('stile')
  <style>
    .my-custom-scrollbar {
      position: relative;
      height: 380px;
      overflow: auto;
      }

    .table-wrapper-scroll-y {
      display: block;
      }
  </style>  
@endsection

@section('content')

@section('navbar-title')Clienti @endsection

<!-- Messaggio di successo se presente -->
@if(Session::has('messaggio') )
<div class="container">
  <div class="col-sm-12">
    <div class="alert alert-success alert-dismissible fade show text-center" id="success-alert">{{ Session::get('messaggio') }}</div>
  </div>
</div>
@endif
<!-- Messaggio di errore se il form non è andato a buon fine -->
@if ($errors->any())
<div class="container">
    <div class="col-sm-12">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
</div>  
@endif

<!-- Titolo e pulsante + -->
<div class="container"> 
  <div style="background-color: #79a3b1"  class="d-flex  p-3 my-3 rounded shadow-sm">
    <div class="lh-100">
      <h1 class="mb-0 lh-100" style="font-size:32px">Tutti i clienti
    <a href="{{ URL::action('ClientController@create') }}" data-toggle="tooltip" title="Nuovo Cliente">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16">
        <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
        <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
      </svg>
    </a>
  </h1>
</div>
</div> 
<hr>
</div>

<div class="container">
  <div class="card shadow">
  <div class="table-wrapper-scroll-y my-custom-scrollbar">
  
      <table class="table table-striped table-hover">
        
        <thead class="text-white" style="background-color: #456268">
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">Cognome</th>
            <th scope="col">Email</th>
            <th scope="col">Ragione Sociale</th>
            <th scope="col"></th>
          </tr>
        </thead>


        <tbody>

        @foreach ($clients as $c)
          <tr>
            <td>{{ $c->nome }}</td>
            <td>{{ $c->cognome }}</td>
            <td>{{ $c->email }}</td>
            <td>{{ $c->ragione_sociale }}</td>
            
            <td><a href="{{ URL::action('ClientController@edit', $c) }}" data-toggle="tooltip" title="Modifica Cliente"><svg width="16" height="16" fill="#0d6efd" >
              <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
            </svg></a></td>
          
          </tr>
        @endforeach
        
        </tbody>
      
    </table>
  </div>
</div>
</div>

@endsection



@section('script')
  <script>
    $(document).ready(function(){
        $("#success-alert").delay(1500).slideUp(280, function() {
            $(this).alert('close');
            });
        $('[data-toggle="tooltip"]').tooltip();
        });
  </script>    
@endsection