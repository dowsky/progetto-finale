@extends('layouts.app')

@section('titolo')
  <title>Nuovo Cliente</title>  
@endsection

@section('stile')
  <style>.asterisco-rosso{ color: red } </style>
@endsection

@section('content')

@section('navbar-title')Nuovo Cliente @endsection

<div class="container">
    <h1 class="text-center">Nuovo Cliente</h1>
    <div class="row">

      
        <div class="col-sm">
        </div>


          <div class="col-sm">
            <form id="form" action="{{ URL::action('ClientController@store')}}" method="POST">
              
              <!-- il valore csrf_field() serve per non permettere l'esecuzione di form non generati da laravel -->
              @csrf
              
              <div class="form-group">
                  <label>Nome cliente<span class="asterisco-rosso">*</span></label>
                  <input type="text" class="form-control" name="nome" required>
                </div>
                <div class="form-group">
                  <label>Cognome cliente<span class="asterisco-rosso">*</span></label>
                  <input type="text" class="form-control" name="cognome" required> 
                </div>
                <div class="form-group">
                  <label>Ragione sociale</label>
                  <input type="text" class="form-control" name="ragione_sociale">
                </div>
                <div class="form-group">
                  <label>Indirizzo email<span class="asterisco-rosso">*</span></label>
                  <input type="email" class="form-control" name="email" required>
                  <small class="form-text text-muted">esempio@mail.org</small>
                </div>
                <button type="submit" id="bottone" class="btn btn-primary">Aggiungi</button>
            </form>
          </div>


        <div class="col-sm" >
        </div>


    </div>
</div>
@endsection
